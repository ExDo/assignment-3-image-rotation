#include <stdio.h>

#include "bmp.h"
#include "fhandler.h"
#include "transformations.h"

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <input file> <output file>)", argv[0]);
        return 0;
    }

    struct image img = {0};
    FILE *input_file = open_file(argv[1], "rb");
    if (input_file == NULL) return 0;
    enum read_status rs = from_bmp(input_file, &img);
    close_file(input_file, argv[1]);
    if (rs != READ_OK) {
        struct_image_destroy(&img);
        fprintf(stderr, "Can't read image from file %s\nReading error code: %d\n", argv[1], rs);
        return 0;
    }

    struct image rotated_img = rotate(img);
    struct_image_destroy(&img);
    if (!rotated_img.data) {
        fprintf(stderr, "Can't rotate image\n");
        return 0;
    }

    FILE *output_file = open_file(argv[2], "wb");
    if (output_file == NULL) return 0;
    enum write_status ws = to_bmp(output_file, &rotated_img);
    close_file(output_file, argv[2]);
    struct_image_destroy(&rotated_img);
    if (ws != WRITE_OK) {
        fprintf(stderr, "Can't write image to file %s\n", argv[2]);
        return 0;
    }
    printf("Image was successfully rotated and saved to %s", argv[2]);

    return 0;
}
