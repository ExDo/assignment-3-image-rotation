#include "transformations.h"

#include <inttypes.h>
#include <stdlib.h>

struct image rotate(struct image const source) {
    struct image img = struct_image_create(source.height, source.width);
    if (!img.data)
        return img;
    for (uint64_t h = 0; h < img.height; h++)
        for (uint64_t w = 0; w < img.width; w++)
            img.data[h * img.width + w] = source.data[(source.height - w - 1) * source.width + h];
    return img;
}
