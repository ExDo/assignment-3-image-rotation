#include "fhandler.h"

#include <errno.h>
#include <stdlib.h>
#include <string.h>

FILE *open_file(const char *filename, const char *mode) {
    FILE *file = fopen(filename, mode);
    if (file != NULL) return file;
    fprintf(stderr, "Can't open file \"%s\"\n", filename);
    fprintf(stderr, "Error: %s\n", strerror(errno));
    return NULL;
}

int close_file(FILE *file, const char *filename) {
    if (fclose(file) == 0) return 0;
    if (filename != NULL)
        fprintf(stderr, "Can't close file \"%s\"\n", filename);
    else
        fprintf(stderr, "Can't close file\n");
    fprintf(stderr, "Error: %s\n", strerror(errno));
    return 1;
}
