#include "image.h"

#include <stdlib.h>

struct image struct_image_create(uint64_t width, uint64_t height) {
    return (struct image){width, height, malloc(width * height * sizeof(struct pixel))};
}

void struct_image_destroy(struct image *img) {
    if (img->data) {
        free(img->data);
        img->data = NULL;
    }
}
