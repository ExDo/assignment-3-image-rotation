#include "bmp.h"

#include <inttypes.h>
#include <stdlib.h>

#define BMP_SIGNATURE 0x4D42
#define BMP_RESERVED 0
#define BMP_DIB_HEADER_SIZE 40
#define BMP_DEFAULT_PLANES 1
#define BMP_NO_COMPRESSION 0
#define BMP_DEFAULT_BITS 24
#define BMP_DEFAULT_HORIZ_RES 1000
#define BMP_DEFAULT_VERT_RES 1000
#define BMP_DEFAULT_COLORS 0
#define BMP_DEFAULT_IMPORTANT_COLORS 0

struct __attribute__((packed)) bmp_header {
    // BITMAPFILEHEADER
    uint16_t bfType;      // File Type
    uint32_t bfileSize;   // File Size
    uint32_t bfReserved;  // Reserved, must be 0
    uint32_t bOffBits;    // Offset to image data
    // BITMAPINFOHEADER
    uint32_t biSize;           // Size of header
    uint32_t biWidth;          // Width of image
    uint32_t biHeight;         // Height of image
    uint16_t biPlanes;         // Number of color planes, must be 1
    uint16_t biBitCount;       // Number of bits per pixel, 24 for now
    uint32_t biCompression;    // Type of compression to use, 0 for now
    uint32_t biSizeImage;      // Size of image data
    uint32_t biXPelsPerMeter;  // X pixels per meter
    uint32_t biYPelsPerMeter;  // Y pixels per meter
    uint32_t biClrUsed;        // Color table size
    uint32_t biClrImportant;   // Number of important colors
};

static inline uint8_t bmp_row_padding(uint32_t const width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

static inline enum read_status bmp_header_validate(struct bmp_header const *const header) {
    if (header->bfType != BMP_SIGNATURE) return READ_INVALID_SIGNATURE;
    if (header->bfReserved != BMP_RESERVED) return READ_INVALID_HEADER;
    if (header->biSize != BMP_DIB_HEADER_SIZE) return READ_INVALID_HEADER;
    if (header->biPlanes != BMP_DEFAULT_PLANES) return READ_INVALID_PLANES;
    if (header->biCompression != BMP_NO_COMPRESSION) return READ_INVALID_COMPRESSION;
    if (header->biBitCount != BMP_DEFAULT_BITS) return READ_INVALID_BITS;
    if (header->bOffBits < sizeof(struct bmp_header)) return READ_INVALID_OFFSET;
    return READ_OK;
}

static inline struct bmp_header struct_bmp_header_create(uint32_t width, uint32_t height) {
    size_t const IMG_SIZE = width * height * sizeof(struct pixel) + height * bmp_row_padding(width);
    struct bmp_header header = {
        .bfType = BMP_SIGNATURE,
        .bfReserved = BMP_RESERVED,
        .bfileSize = sizeof(struct bmp_header) + IMG_SIZE,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = BMP_DIB_HEADER_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = BMP_DEFAULT_PLANES,
        .biBitCount = BMP_DEFAULT_BITS,
        .biCompression = BMP_NO_COMPRESSION,
        .biSizeImage = IMG_SIZE,
        .biXPelsPerMeter = BMP_DEFAULT_HORIZ_RES,
        .biYPelsPerMeter = BMP_DEFAULT_VERT_RES,
        .biClrUsed = BMP_DEFAULT_COLORS,
        .biClrImportant = BMP_DEFAULT_IMPORTANT_COLORS,
    };
    return header;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header = {0};
    size_t read_obj_count = fread(&header, sizeof(header), 1, in);
    if (read_obj_count != 1) return READ_INVALID_FILE;
    enum read_status const status = bmp_header_validate(&header);
    if (status != READ_OK) return status;

    uint8_t const PADDING = bmp_row_padding(header.biWidth);
    *img = struct_image_create(header.biWidth, header.biHeight);
    if (!img->data) {
        if (!img->height) return READ_OK;
        return READ_INVALID_FILE;
    }

    int fseek_result = fseek(in, header.bOffBits, SEEK_SET);
    if (fseek_result != 0) return READ_INVALID_FILE;
    for (uint64_t i = 0; i < img->height; i++) {
        read_obj_count = fread(img->data + i * img->width, sizeof(struct pixel), img->width, in);
        if (read_obj_count != img->width) return READ_INVALID_FILE;
        fseek_result = fseek(in, PADDING, SEEK_CUR);
        if (fseek_result != 0) return READ_INVALID_FILE;
    }
    return READ_OK;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    struct bmp_header header = struct_bmp_header_create(img->width, img->height);
    size_t written_obj_count = fwrite(&header, sizeof(header), 1, out);
    if (written_obj_count != 1) return WRITE_ERROR;
    uint8_t const PADDING = bmp_row_padding(img->width);
    int fseek_result;
    for (uint64_t i = 0; i < img->height - 1; i++) {
        written_obj_count = fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        if (written_obj_count != img->width) return WRITE_ERROR;
        fseek_result = fseek(out, PADDING, SEEK_CUR);
        if (fseek_result != 0) return WRITE_ERROR;
    }
    written_obj_count = fwrite(img->data + (img->height - 1) * img->width, sizeof(struct pixel), img->width, out);
    if (written_obj_count != img->width) return WRITE_ERROR;
    written_obj_count = fwrite(img->data, PADDING, 1, out);
    if (written_obj_count != 1) return WRITE_ERROR;

    return WRITE_OK;
}
