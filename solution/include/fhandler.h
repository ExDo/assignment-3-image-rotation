#pragma once
#include <stdio.h>

FILE *open_file(const char *filename, const char *mode);

// File name is not needed here, you can use NULL
int close_file(FILE *file, const char *filename);
