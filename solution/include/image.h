#pragma once

#include <inttypes.h>
struct pixel {
    uint8_t b, g, r;
};

struct __attribute__((packed)) image {
    uint64_t width, height;
    struct pixel *data;
};

struct image struct_image_create(uint64_t width, uint64_t height);

void struct_image_destroy(struct image *img);
